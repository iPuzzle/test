package ua.com.ipuzzle.myapplication.di

import com.google.gson.Gson
import ua.com.ipuzzle.myapplication.data.remote.NetworkMonitor
import ua.com.ipuzzle.myapplication.data.remote.NoNetworkException
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import ua.com.ipuzzle.myapplication.BuildConfig
import ua.com.ipuzzle.myapplication.data.remote.ApiInterface
import ua.com.ipuzzle.myapplication.data.remote.ApiManager
import ua.com.ipuzzle.myapplication.ui.main.MainViewModel
import java.util.concurrent.TimeUnit

private fun getLoggingInterceptor(): HttpLoggingInterceptor {

    return HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { msg ->
        if (BuildConfig.DEBUG) {

            if (msg.length > 2048) {
                msg.split("\n").forEach {
                    Timber.tag("Retrofit").d(it)
                }
            } else {
                Timber.tag("Retrofit").d(msg)
            }
        }
    }).apply {
        level = if (BuildConfig.DEBUG)
            HttpLoggingInterceptor.Level.BODY
        else
            HttpLoggingInterceptor.Level.HEADERS

    }
}

private fun getOkHttp(loggingInterceptor: HttpLoggingInterceptor, monitor: NetworkMonitor): OkHttpClient {
    return OkHttpClient.Builder()
        .addInterceptor(loggingInterceptor)
        .addInterceptor { chain ->
            if (monitor.isConnected) {
                chain.proceed(chain.request())
            } else {
                throw NoNetworkException()
            }
        }
        .readTimeout(1, TimeUnit.MINUTES)
        .connectTimeout(1, TimeUnit.MINUTES)
        .build()
}

private fun getRestAdapter(okHttpClient: OkHttpClient): ApiInterface {

    return Retrofit.Builder()
        .baseUrl("http://api.timezonedb.com/")
        .addConverterFactory(GsonConverterFactory.create(Gson()))
        .client(okHttpClient)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(ApiInterface::class.java)
}

val appModule = module {

    viewModel { MainViewModel(get()) }

    single { NetworkMonitor(androidApplication()) }
    single { getLoggingInterceptor() }
    single { getOkHttp(get(), get()) }
    single { getRestAdapter(get()) }
    single { ApiManager(get()) }
}

val myApplicationModule = listOf(appModule)