package ua.com.ipuzzle.myapplication

import android.app.Application
import org.koin.android.ext.android.startKoin
import timber.log.Timber
import ua.com.ipuzzle.myapplication.di.myApplicationModule

class App: Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())

        startKoin(this, myApplicationModule)
    }
}