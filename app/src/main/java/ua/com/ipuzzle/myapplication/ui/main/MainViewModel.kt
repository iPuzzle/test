package ua.com.ipuzzle.myapplication.ui.main

import androidx.lifecycle.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import ua.com.ipuzzle.myapplication.data.remote.ApiManager
import ua.com.ipuzzle.myapplication.data.remote.NoNetworkException
import ua.com.ipuzzle.myapplication.data.remote.model.Zone

class MainViewModel(
    private val apiManager: ApiManager
) : ViewModel(), LifecycleObserver {

    private val disposables = CompositeDisposable()

    val timezones = MutableLiveData<List<Zone>>()
    val errorMessage = MutableLiveData<String>()

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun getTimeZones() {

        disposables += apiManager.getTimeZones()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ list ->

                timezones.value = list.sortedBy { it.zoneName }

            }, { throwable ->

                errorMessage.value = if (throwable is NoNetworkException) {
                    "Please enable internet connection and restart application"
                } else {
                    throwable.localizedMessage
                }
                Timber.i(throwable)

            })
    }

    override fun onCleared() {

        disposables.dispose()
    }
}