package ua.com.ipuzzle.myapplication.data.remote

import android.content.Context
import android.net.ConnectivityManager

class NetworkMonitor(private val context: Context)  {

    val isConnected: Boolean
        get() {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnected
        }

}