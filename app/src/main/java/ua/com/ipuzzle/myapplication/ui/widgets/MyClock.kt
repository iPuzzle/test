package ua.com.ipuzzle.myapplication.ui.widgets

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import ua.com.ipuzzle.myapplication.R
import java.util.*


class MyClock(
    context: Context?,
    private val attrs: AttributeSet?,
    defStyleAttr: Int
) : View(context, attrs, defStyleAttr) {

    private val rect = Rect()
    private var padding = 0
    private var fontSize = 0f
    private var radius = 0
    private var paint: Paint = Paint()
    private var handTruncation = 0
    private var hourHandTruncation = 0
    var clockColor: Int = resources.getColor(android.R.color.black)
    var timeZoneName = Calendar.getInstance().timeZone.displayName!!
    private var handWidth = 8.0f
    private var secondsHandWidth = 2.0f

    var clockCorrectionDelta: Long = 0

    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context?) : this(context, null)

    init {

        initClock()
    }

    private fun initClock() {

        padding = 100
        fontSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 17.0f, resources.displayMetrics)
        handWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4.0f, resources.displayMetrics)
        secondsHandWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1.0f, resources.displayMetrics)

        if (attrs != null) {
            val ta = context.obtainStyledAttributes(attrs, R.styleable.MyClock)
            clockColor = ta.getColor(R.styleable.MyClock_clock_color, Color.BLACK)
            paint.color = clockColor
            ta.recycle()
        }

        paint.color = clockColor

        resize()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        resize()
    }

    private fun resize() {

        val min = Math.min(width, height)
        radius = min / 2 - padding
        handTruncation = min / 20
        hourHandTruncation = min / 7

    }

    private fun drawCircle(canvas: Canvas) {

        paint.apply {
            reset()
            color = clockColor
            strokeWidth = 5.0f
            style = Paint.Style.STROKE
            isAntiAlias = true

            canvas.drawCircle((width / 2).toFloat(), (height / 2).toFloat(), (radius + padding - 10).toFloat(), this)
        }
    }

    private fun drawNumbers(canvas: Canvas) {

        paint.textSize = fontSize

        for (number in 1..12) {
            val tmp = number.toString()
            paint.getTextBounds(tmp, 0, tmp.length, rect)
            val angle = Math.PI / 6 * (number - 3)
            val x = (width / 2 + Math.cos(angle) * radius - rect.width() / 2).toFloat()
            val y = (height / 2 + Math.sin(angle) * radius + rect.height() / 2).toFloat()
            canvas.drawText(tmp, x, y, paint)
        }
    }

    private fun drawCenter(canvas: Canvas) {

        paint.style = Paint.Style.FILL
        canvas.drawCircle((width / 2).toFloat(), (height / 2).toFloat(), 12.0f, paint)
    }

    private fun drawHand(canvas: Canvas, loc: Double, isHour: Boolean, isBold: Boolean) {

        val angle = Math.PI * loc / 30 - Math.PI / 2
        val handRadius = if (isHour) radius - handTruncation - hourHandTruncation else radius - handTruncation

        paint.strokeWidth = if (isBold) {
            handWidth
        } else {
            secondsHandWidth
        }

        canvas.drawLine(
            (width / 2).toFloat(),
            (height / 2).toFloat(),
            (width / 2 + Math.cos(angle) * handRadius).toFloat(),
            (height / 2 + Math.sin(angle) * handRadius).toFloat(),
            paint
        )
    }

    private fun drawHands(canvas: Canvas) {

        val calendar = Calendar.getInstance()
        calendar.timeZone = TimeZone.getTimeZone(timeZoneName)

        var hour = calendar.get(Calendar.HOUR_OF_DAY).toFloat()
        if (hour > 12) hour -= 12.0f

        drawHand(canvas, (hour + calendar.get(Calendar.MINUTE) / 60.0) * 5.0, true, true)
        drawHand(canvas, calendar.get(Calendar.MINUTE).toDouble(), false, true)
        drawHand(canvas, calendar.get(Calendar.SECOND).toDouble(), false, false)
    }

    override fun onDraw(canvas: Canvas?) {

        canvas?.apply {
            drawColor(Color.TRANSPARENT)
            drawCircle(this)
            drawCenter(this)
            drawNumbers(this)
            drawHands(this)

        }

        postInvalidateDelayed(500)
        invalidate()

    }
}