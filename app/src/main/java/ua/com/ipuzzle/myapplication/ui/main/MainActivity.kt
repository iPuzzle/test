package ua.com.ipuzzle.myapplication.ui.main

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import ua.com.ipuzzle.myapplication.R

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()

    private val colorsNames = arrayOf("BLUE", "RED", "GREEN", "YELLOW")
    val colors = arrayOf(
        (0xff0000ee).toInt(),
        (0xffcc0000).toInt(),
        (0xff00cc00).toInt(),
        (0xffeecc00).toInt()
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        lifecycle.addObserver(viewModel)

        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, colorsNames)
        color_spinner.adapter = adapter
        color_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                my_clock.clockColor = colors[position]
            }

        }

        val timezoneAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item)
        timezone_spinner.adapter = timezoneAdapter
        timezone_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                my_clock.clockCorrectionDelta = 0
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                my_clock.timeZoneName = viewModel.timezones.value!![position].zoneName
            }

        }

        viewModel.timezones.observe(this, Observer { list ->

            val newArray = list.map {
                it.zoneName
            }

            timezoneAdapter.clear()
            timezoneAdapter.addAll(newArray)
        })

        viewModel.errorMessage.observe(this, Observer { message ->

            Toast.makeText(this@MainActivity, message, Toast.LENGTH_LONG).show()

        })
    }
}
