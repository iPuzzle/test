package ua.com.ipuzzle.myapplication.data.remote

import io.reactivex.Observable
import ua.com.ipuzzle.myapplication.data.remote.model.Zone

class ApiManager(private val apiInterface: ApiInterface) {

    fun getTimeZones(): Observable<List<Zone>> {

        return apiInterface.getTimeZones("7ZMUX0SQYIVY")
            .map { response ->

                if (response.status != "OK") {
                    throw Throwable(response.message)
                } else {
                    return@map response.zones
                }

            }
    }
}