package ua.com.ipuzzle.myapplication.data.remote.model

data class ApiResponse(
    val status: String,
    val message: String,
    val zones: List<Zone>
)