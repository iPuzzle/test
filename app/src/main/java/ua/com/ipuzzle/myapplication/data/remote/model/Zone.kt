package ua.com.ipuzzle.myapplication.data.remote.model

data class Zone(
    val countryCode: String,
    val countryName: String,
    val zoneName: String,
    val gmtOffset: Long,
    val timestamp: Long
)
