package ua.com.ipuzzle.myapplication.data.remote

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import ua.com.ipuzzle.myapplication.data.remote.model.ApiResponse

interface ApiInterface {

    @GET("/v2.1/list-time-zone")
    fun getTimeZones(@Query("key") key: String,
                     @Query("format") format: String = "json"): Observable<ApiResponse>
}